    <?php
        defined('BASEPATH') OR exit('No direct script access allowed');

        class Search_Model extends CI_Model{

          public function __construct()
            {
              parent::__construct();
              $this->load->database();
            }

          public function cek($tag)
            {
              $this->db->select('*');
              $this->db->from('tb_search_tag');
              $this->db->where('tag',$tag);
              $query = $this->db->get();
              return $query->row();
            }
          
        }
    ?>
    