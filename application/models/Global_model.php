<?php defined('BASEPATH') OR exit('No direct script access allowed');
	

class Global_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function select($table, $rules = [],$field='*'){
		
		$this->db->select($field);
        $this->db->from($table);
        
        if(is_array($rules)){
	        if(isset($rules ['where'])){
		        $this->db->where($rules['where']);
	        }
	        
	        if(isset($rule ['order_by'])){
		        $this->db->order_by($rules['order_by']);
	        }
        }
        
        return $this->db->get();
	}
	
	public function insert($table, $data){
		
		if($this->db->insert($table,$data)){
            return $this->db->insert_id(); 
        }else{
            return false;
        }
	}
	
	public function update($table, $data, $where){
        
        $this->db->where($where);
        $data = $this->db->update($table, $data);
        if($data){
            return true;
        }else{
            return false;
        }
    }

    public function delete($table,$where){
        
        $this->db->where($where);
        $del = $this->db->delete($table);
        if($del){
            return true;
        }else{
            return false;
        }
        
    }
    
    public function query($q){
	    return $this->db->query($q);
    }

}