<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'vendor/autoload.php';
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . 'libraries/Format.php';
    use Restserver\Libraries\REST_Controller;
    use \Firebase\JWT\JWT;
   
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    class Logout extends REST_Controller {
        private $secretkey = '38c7ebf2ae708b746d82b84060b09922';
        function __construct($config = 'rest') {
            parent::__construct($config);
            
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
            $this->load->database();
            $this->load->model('Login_Model');
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
        }
        public function viewtokenfail($username){
            return $this->response(array('Status'=>False, 'message'=>'Fail Generated'), 502);
        }

        public function cektoken(){

            $this->load->model('Login_Model');
            $jwt = $this->input->get_request_header('Authorization');

            try {
                $decode = JWT::decode($jwt,$this->secretkey,array('HS256'));

                if ($this->Login_Model->is_valid_num($decode->username)>0) {
                    return true;
                }

            } catch (Exception $e) {
                return $this->response(array('status' => false ,'message' => 'Invalid Token',),502);
            }
        }

    public function index_post(){
    //     $this->cektoken();
            $this->load->model('Login_Model');
            $jwt = $this->input->get_request_header('Authorization');

            try {
                $decode = JWT::decode($jwt,$this->secretkey,array('HS256'));

                if ($this->Login_Model->is_valid_num($decode->username)>0) {
                    $payload['id']       = $decode->id;
                    $payload['username'] = $decode->username;
                    $output ['token']    = array(JWT::encode($payload,$this->secretkey));
                   return $this->response(array('status' => true ,'message' => 'succes logout',),200);
                }

            } catch (Exception $e) {
                 return $this->response(array('status' => true ,'message' => 'invalid token',),502);
            }
        }

    }
    ?>