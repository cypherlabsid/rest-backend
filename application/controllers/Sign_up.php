<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'vendor/autoload.php';
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . 'libraries/Format.php';
    use Restserver\Libraries\REST_Controller;
    use \Firebase\JWT\JWT;
   
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    class Sign_up extends REST_Controller {
        private $secretkey = '38c7ebf2ae708b746d82b84060b09922';
        function __construct($config = 'rest') {
            parent::__construct($config);
            
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
            $this->load->database();
            $this->load->model('Login_Model');
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
        }

        function sign_up_get($id = '') {
            // $this->cektoken();
                $table = 'tb_user';
                $get_id = 'id';
                if ($id == '') {
                    $data_load = $this->db->get($table)->result();
                } else {
                
                    $this->db->where($get_id, $id);
                    $data_load = $this->db->get($table)->result();
                }
                $this->response(array('status'=>true,'message'=>'success loaded','data' =>$data_load), 200);
            
        }

        function index_post() { 
            
            $table = 'tb_user';
            $date  = date("Y-m-d H:i:s");
            $pass  = $this->post('password');
            $md5   = md5($pass);
            $username = $this->post('username');
            $data  = [ 
                        'username'  => $username,
                        'first_name'=> $this->post('first_name'),
                        'last_name' => $this->post('last_name'),
                        'created'   => $date,
                        'email'     => $this->post('email'),
                        'password'  => $md5 
                     ];
            $validate = $this->Login_Model->is_valid($username); 
            $this->form_validation->set_rules('username', 'Tag', 'required'); 
            $this->form_validation->set_rules('first_name', 'First_name', 'required'); 
            $this->form_validation->set_rules('last_name', 'Last_name', 'required'); 
            $this->form_validation->set_rules('email', 'Email', 'required'); 
            $this->form_validation->set_rules('password', 'Password', 'required'); 
           if ($this->form_validation->run() == true) { 
              if ($validate){  
                return $this->response(array('status'=>false, 'message'=>'username already exists'), 502);
                }else {
                    $insert = $this->db->insert($table,$data);
                    $id = $this->db->insert_id();
                    if ($insert) {
                            $data_insert = $this->post();
                    $this->response(array('status'=>true,'message'=>'success insert','data' => [$data_insert],), 200);
                    } 
                    else {
                            $this->response(array('status' => false, 'message'=>'insert failed', 'data' => [$data_insert],),520);
                         }
                    } 
                }
                    else {
                            return $this->response(array('status'=>false, 'message'=>'please complete data contents'), 502);
                         }
                
             }
        function index_put($id = '') {
            //$this->cektoken();
            $table = 'tb_user';
            $get_id = 'id';
            $this->db->where($get_id, $id);
            $update = $this->db->update($table, $this->put());
            if ($update) {
                $data_update = $this->put();
                $this->response(array('status' => true, 'message' => 'success update','data' => [$data_update],), 200);
            } 
                else {
                    $this->response(array('status' => false,'message' => 'update failed','data' => [$data_update],), 502);
                }
        }

        function sign_up_delete($id) {
          //  $this->cektoken();
            $get_id = 'id';
            $table  = 'tb_user';
            $this->db->where($get_id, $id);
            $delete = $this->db->delete($table);
            if ($delete) {
                $data_delete['id'] = $id;
                $this->response(array('status' => true, 'message' => 'success delete','data' => [$data_delete],), 200);
            } 
                else {
                    $this->response(array('status' => false,'message' => 'delete failed','data' => [$data_update],), 502);
                }
        }

    }
    ?>