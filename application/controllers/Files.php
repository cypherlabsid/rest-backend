<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

    public function images($name)
    {
        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
        header ('Content-type: image/png');

        readfile(FCPATH . 'upload/images/tmp/' . $name);


    }

    public function manual_compress(){

	    $page = $this->input->get('page');
	    $data = $this->pages($page);
	    echo count($data);
	    foreach($data as $r){

            $origin = FCPATH . 'upload/images/' . $r->image_file;
            $dest = FCPATH . 'upload/images/tmp/'.$r->image_file;
            if(file_exists($origin)){
                $this->convertPNGto8bitPNG($origin,$dest);
            }else{
                echo "{$r->id} file tidak ada ".$r->image_file."<br>";
            }

        }

    }

    private function pages($page){
	    $limit = 50;
	    $tot_data = $this->db->query("SELECT COUNT(*) FROM tb_contact_image");

	    $offset = ($page-1)*50;

	    $data  = $this->db->query("SELECT * FROM tb_contact_image LIMIT $offset, $limit");

	    return $data->result();
    }



    function convertPNGto8bitPNG ($sourcePath, $destPath) {

        $srcimage = imagecreatefrompng($sourcePath);
        list($width, $height) = getimagesize($sourcePath);

        $img = imagecreatetruecolor($width, $height);
        $bga = imagecolorallocatealpha($img, 0, 0, 0, 127);
        imagecolortransparent($img, $bga);
        imagefill($img, 0, 0, $bga);
        imagecopy($img, $srcimage, 0, 0, 0, 0, $width, $height);
        imagetruecolortopalette($img, false, 255);
        imagesavealpha($img, true);

        imagepng($img, $destPath);
        imagedestroy($img);

    }

}

/* End of file Controllername.php */