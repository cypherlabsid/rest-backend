<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'vendor/autoload.php';
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . 'libraries/Format.php';
    use Restserver\Libraries\REST_Controller;
    use \Firebase\JWT\JWT;
   
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    class Login extends REST_Controller {
        private $secretkey ="";
        function __construct($config = 'rest') {
            parent::__construct($config);
            
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
            $this->load->database();
            $this->load->model('Login_Model');
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
        }

        public function index_post(){
            
            $date  = new DateTime();
            $username = $this->post('username',TRUE); 
            $password = $this->post('password',TRUE); 
            $dataadmin = $this->Login_Model->is_valid($username);

            if ($dataadmin) {
                $pass = $this->post('password');
                $md5 = md5($pass);
                if (password_verify($md5,password_hash($dataadmin->pass, PASSWORD_DEFAULT))) {
                    $payload['id'] = $dataadmin->id;
                    $payload['username'] = $dataadmin->username;
                    $output['token'] = JWT::encode($payload,$this->secretkey);
                    $data = array('status'=>true,
                                 'message'=>'succsess login', 
                                 'data'=>$output);
                    
                    $this->set_response($data, REST_Controller::HTTP_OK);

                } else {
                    return $this->response(array('status'=>false, 'message'=>'password wrong', 'data'=>[$md5],), 502);
                }
                } else {
                return $this->response(array('status'=>false, 'message'=>'username wrong'), 502);
                }
            
        }

    }
    ?>