<?php
	header('Access-Control-Allow-Origin: *'); 
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	header('Access-Control-Allow-Headers: Authorization');
	
	class Accounts extends MY_Controller{
		
		public function __construct(){
			parent::__construct(true);
		}
		
		public function change_password_post(){
			
			$this->form_validation->set_rules('old_password',"Old Password", 'trim|required');
			$this->form_validation->set_rules('new_password', 'Password', 'trim|required');
			$this->form_validation->set_rules('confirm_password', "Confirm Password", "trim|required|matches[new_password]");
			
			if($this->form_validation->run() == TRUE){
				if($this->users->check_password($this->user_info->id,$this->post('old_password'), $this->user_info->pass)){
					$new_pass 	= $this->users->hash_password($this->post('new_password'),0);
					
					$updated	= $this->store->update('tb_user',['pass'=> $new_pass], array('id'=> $this->user_info->id));
					if($updated){
						return $this->response(array('status'=>true, 'message'=>"Success, password has been updated."), 200);
					}
					return $this->response(array('status'=>false, 'message'=>"Failed, something has error. please try again."), 502);
				}
				return $this->response(array('status'=>false, 'message'=>"The Old password is not valid."), 502);
			}
			
			return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);	
			
		}
		
		public function update_profile_post(){
			
			$this->form_validation->set_rules('first_name', "First Name", 'trim|required');
			$this->form_validation->set_rules('last_name', "Last Name", 'trim|required');
			$this->form_validation->set_rules('regional',"Regional", "trim|required");
			
			if($this->form_validation->run() == TRUE){
				
				$data = [
					'first_name' 	=> $this->post('first_name'),
					'last_name'		=> $this->post('last_name'),
					'regional'		=> $this->post('regional')
				];
				
				$updated = $this->store->update('tb_user',$data, ['id' => $this->user_info->id]);
				
				if($updated){
					return $this->response(array('status'=>true, 'message'=>"Success, profile has been updated."), 200);
				}
				
				return $this->response(array('status'=>false, 'message'=> "Something has error, please try again"), 502);
			}
			return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);
		}
		
	}