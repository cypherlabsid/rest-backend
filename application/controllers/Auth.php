<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Auth extends MY_Controller{
		
		
		public function __construct(){
			parent::__construct();
		}
		
		public function registration_post(){
			$this->load->library('emailer');
            $data  = [
                'first_name'=> $this->post('first_name'),
                'last_name' => $this->post('last_name')
             ];
             
            $insert = $this->users->create_user($this->post('email'), $this->post('password'), $this->post('username'), $data);
            if($insert){
	            $this->users->login_fast($insert);
	            $user_info = $this->users->get_user();
	            
	            $this->emailer->mailSend($this->post('email'), "Welcome to GEM App",$this->emailer->register_body(['username' => $this->post('username'),'email' => $this->post('email'),'first_name' => $this->post('first_name')]));
	            
	            $this->response(array('status'=>true,'message'=>"User Created",'data' => [$user_info]), 200);
            }else{
	             $this->response(array('status' => false, 'message'=>$this->users->errors[0]),502);
            }
            
		}
		
		public function login_post(){
			
			$username = $this->post('username',TRUE); 
            $password = $this->post('password',TRUE);
            
            $login = $this->users->login($username, $password);
            if($login){
	            $user_info = $this->users->get_user();
	           
                $time = time();
                
                $payload = [
			        'iat'  => $time,
			        'jti'  => $user_info->id,
			        'nbf'  => $time, 
			        'exp'  => strtotime('+30 days'), 
			        'data' => [ 
			            'id'   => $user_info->id, 
			            'username' => $user_info->username,
			        ]
			    ];
                
                unset($user_info->pass);
                
                $output 				= (Array) $user_info;
                $output ['picture']		= strlen($user_info->picture) > 0 ? $user_info->picture : 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
                $output ['exp']			= date('Y-m-d H:i:s',$payload['exp']);
                $output	['token'] 		= MY_Controller::$JWT->encode($payload,$this->secretkey);
                $data 					= ['status'=>true, 'message'=>"Login success", 'data'=>[$output],];
                
				$this->response($data, 200);
            }else{
	            $this->response(array('status'=>false, 'message'=>$this->users->errors[0]), 502);
            }
            
		}
		
		public function verify_email_post(){
			
			$this->load->library('user_agent');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			
			if($this->form_validation->run() == TRUE){
				
				$user = $this->store->select('tb_user',['where' => ['email' => $this->post('email')]]);
				if($user->num_rows() > 0){
					$this->load->library('emailer');
					
					$now =  strtotime('now');
					
					if($user->row()->forgot_exp != "" && $user->row()->forgot_exp >= $now){
						$ver_code = $user->row()->verification_code;
						$request_time = date('d F Y H:i:s', strtotime($user->row()->forgot_time));
					}else{
						$ver_code = mt_rand(10000,99999);
						$request_time = date('Y-m-d H:i:s');
						$data = [
							'forgot_exp' => strtotime('+15 minutes'),
							'verification_code' => $ver_code,
							'forgot_time'	=> $request_time
						];
					
						
						$this->store->update('tb_user',$data,['id' => $user->row()->id]);
					}
					
					$body_data = ['username' => $user->row()->username,
						'email' => $user->row()->email,
						'first_name' => $user->row()->first_name,
						'agent'=>$this->agent->agent_string(), 
						'ver_code'=> $ver_code,
						'time_req' => date('d F Y H:i:s', strtotime($request_time)) ];
					
					$this->emailer->mailSend($this->post('email'), "Reset Password - GEM App",$this->emailer->reset_password($body_data), $user->row()->first_name);
					return $this->response(array('status'=>true, 'message'=>"Success, verification code has been sent to your email"), 200);
					
				}
				
				return $this->response(array('status'=>false, 'message'=> "Not valid email, Please use registered email."), 502);
							
			}
			
			return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);
			
			
		}
		
		
		public function verification_code_post(){
			
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('verification_code', 'Verification Code', 'required');
			
			if($this->form_validation->run() == TRUE){
				
				$user = $this->store->select('tb_user',['where'=> ['email' => $this->post('email')] ]);
				if($user->num_rows() > 0){
					
					$now = strtotime('now');
					$user = $user->row();
					
					$data = ['user_id' => $user->id, 'email'=> $user->email];
					
					if($this->post('verification_code') == $user->verification_code){
						if($user->forgot_exp > $now){
							return $this->response(array('status'=>true, 'message'=>"Success, verification code is valid.", 'data' => $data), 200);
						}
						return $this->response(array('status'=>false, 'message'=> "Verification code is not valid."), 502);	
					}
					return $this->response(array('status'=>false, 'message'=> "Verification code is not valid."), 502);					
				}
				return $this->response(array('status'=>false, 'message'=> "Not valid email, Please use registered email."), 502);
			}
			
			return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);
			
		}
		
		
		public function reset_password_post(){
			
			$this->form_validation->set_rules('user_id','User ID', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('confirm_password', "Confirm Password", "trim|required|matches[password]");
			
			if($this->form_validation->run() == TRUE){
				$user = $this->store->select('tb_user',['where'=>['id' => $this->post('user_id')]]);
				if($user->num_rows() >0 ){
					$new_pass 	= $this->users->hash_password($this->post('password'), 0);
					
					$update 	= $this->store->update('tb_user',['pass'=> $new_pass,'forgot_exp'=> "", 'verification_code'=> ""], ['id'=> $this->post('user_id')]);
					if($update){
						return $this->response(array('status'=>true, 'message'=>"Success, password has been reset."), 200);
					}
					return $this->response(array('status'=>false, 'message'=>"Failed. Something has error please try again."), 502);
				}
				return $this->response(array('status'=>false, 'message'=>"Failed. invalid users"), 502);
			}
			
			return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);
			
		}


        public function reset_password_adm_post(){

            $this->form_validation->set_rules('user_id','User ID', 'trim|required');
            $this->form_validation->set_rules('new_password', 'Password', 'trim|required');

            if($this->form_validation->run() == TRUE){
                $user = $this->store->select('tb_user',['where'=>['md5(id)' => $this->post('user_id')]]);
                if($user->num_rows() >0 ){
                    $new_pass 	= $this->users->hash_password($this->post('new_password'), 0);

                    $update 	= $this->store->update('tb_user',['pass'=> $new_pass,'forgot_exp'=> "", 'verification_code'=> ""], ['id'=> $user->row()->id]);
                    if($update){
                        return $this->response(array('status'=>true, 'message'=>"Success, password has been reset."), 200);
                    }
                    return $this->response(array('status'=>false, 'message'=>"Failed. Something has error please try again."), 502);
                }
                return $this->response(array('status'=>false, 'message'=>"Failed. invalid users"), 502);
            }

            return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);

        }


        public function verify_email_adm_post(){

            $this->load->library('user_agent');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

            if($this->form_validation->run() == TRUE){

                $user = $this->store->select('tb_user',['where' => ['email' => $this->post('email')]]);
                if($user->num_rows() > 0){
                    $this->load->library('emailer');

                    $now =  strtotime('now');



                    $body_data = ['username' => $user->row()->username,
                        'web_url' => "http://web.reportgem.com.s3-website-ap-southeast-1.amazonaws.com/update-password?push=".md5($user->row()->id),
                        'email' => $user->row()->email,
                        'first_name' => $user->row()->first_name,
                        'agent'=>$this->agent->agent_string(),
                        'time_req' => date('d F Y H:i:s') ];

                    $this->emailer->mailSend($this->post('email'), "Reset Password - GEM Contat Report",$this->emailer->reset_password_adm($body_data), $user->row()->first_name);
                    return $this->response(array('status'=>true, 'message'=>"Success, verification code has been sent to your email"), 200);

                }

                return $this->response(array('status'=>false, 'message'=> "Not valid email, Please use registered email."), 502);

            }

            return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);


        }
		
	
		
		
	}