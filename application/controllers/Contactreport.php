<?php
	header('Access-Control-Allow-Origin: *'); 
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	header('Access-Control-Allow-Headers: *');
	
	class Contactreport extends MY_Controller{
		
		public function __construct(){
			parent::__construct(true);
            $this->load->library('encrypt');

		}
		
		
		public function recent_get($page = 1, $perpage = 20){
			
		   
			$data = [];
			
			$query 	= "SELECT c.* FROM tb_contact_reports c ORDER BY date DESC LIMIT 60";
			$rows	= $this->store->query($query);
			foreach($rows->result() as $row){
				$dateISO = new DateTime($row->date);
				
				$r ['contact_id'] = $row->contact_id;
				$r ['title']	= $row->title;
				$r ['purpose']	= $row->purpose;
				$r ['date']		= $dateISO->format(DateTime::ATOM);
				$r ['image']	= $this->_getReportImage($row->contact_id);
				$r ['marketer']	= $row->marketer_name;
				$r ['marketer_country'] = "Indonesia";
				$r ['marketer_position'] = "Marketing Officer";
				$r ['avatar']	= 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
				$r ['tags']		= $this->_tagstoArray($row->tags);
				
				array_push($data, $r);
			}

			$monthly    = $this->store->query("SELECT count(*) as total from tb_search WHERE MONTH(date) = MONTH(NOW())")->row()->total;
			$all_time   = $this->store->query("SELECT count(*) as total from tb_search ")->row()->total;
            $chart ['monthly'] = $this->num_format($monthly);
            $chart ['all_time'] = $this->num_format($all_time);

			$response = [
				'status'	=> true,
				'message'	=> "Contact report loaded",
				'data'		=> $data,
                'chart'     => $chart
			];
			
			return $this->response($response, 200);
		}

        public function recent_web_get($page = 1, $perpage = 20){


            $data = [];

            $query 	= "SELECT c.* FROM tb_contact_reports c ORDER BY date_created DESC LIMIT 100";
            $rows	= $this->store->query($query);
            foreach($rows->result() as $row){
                $dateISO = new DateTime($row->date);

                $r ['contact_id'] = $row->contact_id;
                $r ['title']	= $row->title;
                $r ['purpose']	= $row->purpose;
                $r ['date']		= $dateISO->format(DateTime::ATOM);
                $r ['image']	= $this->_getReportImage($row->contact_id);
                $r ['marketer']	= $row->marketer_name;
                $r ['marketer_country'] = "Indonesia";
                $r ['marketer_position'] = "Marketing Officer";
                $r ['avatar']	= 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
                $r ['tags']		= $this->_tagstoArray($row->tags);

                array_push($data, $r);
            }

            $monthly    = $this->store->query("SELECT count(*) as total from tb_search WHERE MONTH(date) = MONTH(NOW())")->row()->total;
            $all_time   = $this->store->query("SELECT count(*) as total from tb_search ")->row()->total;
            $chart ['monthly'] = $this->num_format($monthly);
            $chart ['all_time'] = $this->num_format($all_time);

            $response = [
                'status'	=> true,
                'message'	=> "Contact report loaded",
                'data'		=> $data,
                'chart'     => $chart
            ];

            return $this->response($response, 200);
        }


		private function num_format($num){
		    if(strlen($num) > 3){
		        $num = ($num/1000). "K";
            }else if(strlen($num) > 6){
                $num = ($num/1000000). "M";
            }

            return $num;
        }
		
		
		public function all_get(){
			$data = [];
			
			$query 	= "SELECT c.* FROM tb_contact_reports c ORDER BY date DESC";
			$rows	= $this->store->query($query);
			foreach($rows->result() as $row){
				$dateISO = new DateTime($row->date);

				$r ['contact_id'] = $row->contact_id;
				$r ['title']	= $row->title;
				$r ['purpose']	= $row->purpose;
				$r ['date']		= $dateISO->format(DateTime::ATOM);
				$r ['image']	= $this->_getReportImage($row->contact_id);
				$r ['marketer']	= $row->marketer_name;
				$r ['marketer_country'] = "Indonesia";
				$r ['marketer_position'] = "Marketing Officer";
				$r ['avatar']	= 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
				$r ['tags']		= $this->_tagstoArray($row->tags);
				
				array_push($data, $r);
			}
			
			$response = [
				'status'	=> true,
				'message'	=> "Contact report loaded",
				'data'		=> $data
			];
			
			return $this->response($response, 200);
		}
		
		public function last_view_get(){
			$id = $this->get('id');
			
			if(strlen($id) > 0){
				$arr = explode(',', $id);
				$orderby = "";
				if(count($arr) > 1){
					krsort($arr);
					$order = "";
					$i = 1;
					
					foreach($arr as $s){
						$order .= " WHEN '$s' THEN $i ";
						$i++;
					}
					if($order != ""){
						$orderby = "ORDER BY (CASE contact_id $order END ) ASC";
					}
				}
				
				$where = "where contact_id in ($id) $orderby";
			}else{
				$where = "ORDER BY date_created DESC LIMIT 10";
			}
			
			$data = [];
			
			$query 	= "SELECT c.* FROM tb_contact_reports c $where";
		
			$rows	= $this->store->query($query);
			foreach($rows->result() as $row){
				$dateISO = new DateTime($row->date);
				
				$r ['contact_id'] = $row->contact_id;
				$r ['title']	= $row->title;
				$r ['purpose']	= $row->purpose;
				$r ['date']		= $dateISO->format(DateTime::ATOM);
				$r ['image']	= $this->_getReportImage($row->contact_id);
				$r ['marketer']	= $row->marketer_name;
				$r ['marketer_country'] = "Indonesia";
				$r ['marketer_position'] = "Marketing Officer";
				$r ['avatar']	= 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
				$r ['tags']		= $this->_tagstoArray($row->tags);
				
				array_push($data, $r);
			}
			
			$response = [
				'status'	=> true,
				'message'	=> "Contact report loaded",
				'data'		=> $data
			];
			
			return $this->response($response, 200);
		}

		public function recent_add_get(){
			
			$data = [];
			
			$query 	= "SELECT c.*, m.first_name, m.last_name, m.picture FROM tb_contact_reports c, tb_user m WHERE m.id = c.marketer_id ORDER BY date DESC LIMIT 1";
			$rows	= $this->store->query($query);
			foreach($rows->result() as $row){
				$dateISO = new DateTime($row->date);
				
				$r ['contact_id'] = $row->contact_id;
				$r ['title']	= $row->title;
				$r ['purpose']	= $row->purpose;
				$r ['date']		= $dateISO->format(DateTime::ATOM);
				$r ['image']	= $this->_getReportImage($row->contact_id);
				$r ['marketer']	= $row->first_name." ".$row->last_name;
				$r ['marketer_country'] = "Indonesia";
				$r ['marketer_position'] = "Marketing Officer";
				$r ['avatar']	= strlen($row->picture) > 0 ? $row->picture : 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
				$r ['tags']		= $this->_tagstoArray($row->tags);
				
				array_push($data, $r);
			}
			
			$response = [
				'status'	=> true,
				'message'	=> "Contact report loaded",
				'data'		=> $data
			];
			
			return $this->response($response, 200);
		}
		
		public function preview_get(){
			$id = $this->get('id');
			$data = $this->store->select('tb_contact_reports',['where' => ['contact_id' => $id]]);
			
			if($data->num_rows() > 0){
				$row = $data->row();
				$r ['contact_id'] 	= $row->contact_id;
				$r ['title']		= $row->title;
				$r ['tags']			= $row->tags;
				$r ['marketer']		= $row->marketer_name;
				$r ['purpose']		= $row->purpose;
				$r ['full_text']	= $row->full_text;
				$r ['date']			= date('Y-m-d',strtotime($row->date));
				$r ['image']		= $this->_getReportImage($row->contact_id);
				return $this->response(['status'=>true,"message"=>"Preview loaded", "data"=> $r],200);
			}else{
				return $this->response(['status'=>false,"message"=>"Invalid Contact Id", "data"=> []],502);
			}
		}
		
		
		public function single_contact_post(){
			
			$this->form_validation->set_rules('title',"Title","trim|required");
			$this->form_validation->set_rules('purpose','Purpose',"trim|required");
			$this->form_validation->set_rules('images',"Contact Report File","trim|required");
			$this->form_validation->set_rules('tags',"Tags","trim|required");
			
			if($this->form_validation->run() == TRUE){
				$data = [
					'title'		=> $this->post('title'),
					'purpose'	=> $this->post('purpose'),
					'tags'		=> $this->post('tags'),
					'full_text'	=> $this->post('contact'),
					'marketer_name'=> $this->post('marketer'),
					'file_origin' => $this->post('file_origin'),
					'date'		=> $this->post('date'),
					'date_created'		=> date('Y-m-d H:i:s')
				];
				
				if(!$this->_valid_uuid(json_encode($this->input->post()))){
					return $this->response(['status'=> false,'message'=> "Success Uploaded"], 200);
				}
				
				$id = $this->store->insert('tb_contact_reports',$data);
				
				$images = explode(',', $this->post('images'));
				foreach($images as $img){
					$imgs = [
						'contact_id' 	=> $id,
						'image_file'	=> $img
					];
					
					$this->store->insert('tb_contact_image',$imgs);
				}
				
				return $this->response(['status'=> false,'message'=> "Success Uploaded"], 200);
			}else{
				 return $this->response(['status'=> false,'message'=> validation_errors()], 502);
			}
		}
		
		
		public function search_tags_post(){
			$this->search_get('tags');
		}
		
		public function search_tags_keyword_get(){
			$keyword = $this->get('keyword');
			$q = "SELECT *
						FROM tb_tags
						WHERE
						    MATCH(tags) AGAINST('$keyword' IN BOOLEAN MODE) OR tags REGEXP '^.*[$keyword]{4}.*$'
						ORDER BY MATCH(tags) AGAINST('$keyword' IN BOOLEAN MODE) DESC
						LIMIT 0, 5";
						
			$data = [];
			$rows	= $this->store->query($q);
			foreach($rows->result() as $row){
				$r ['tags'] = $row->tags;
				array_push($data, $r);
			}
			$response = [
				'status'	=> true,
				'message'	=> "Tags results ",
				'data'		=> $data
			];
			
			return $this->response($response, 200);
		}
		
		
		public function search_get($by){
			
			$keyword = $this->get('keyword');
			
			switch($by){
				case 'company' :
					$q = "SELECT c.*
						FROM tb_contact_reports c
						WHERE
							 c.title LIKE '%$keyword%' ORDER BY c.date DESC
						LIMIT 0, 10";
				break;
				case 'marketer':
					$q = "SELECT c.*
						FROM tb_contact_reports c
						WHERE
						   c.marketer_name LIKE '%$keyword%' ORDER BY c.date DESC
						LIMIT 0, 200";
				break;
				case 'tags' : 
					$rawData = file_get_contents("php://input");
					$arr = json_decode($rawData);
					
					$where = "";
					foreach($arr as $rs){
						if($where == ""){
							$where .= " FIND_IN_SET('$rs->label',tags)";
						}else{
							$where .= " AND FIND_IN_SET('$rs->label',tags)";
						}
					}
					
					$q = "SELECT c.*
						FROM tb_contact_reports c
						WHERE
							
						    $where
						LIMIT 0, 10";
					
				break;
				default:
					$q = null;
				break;
			
			}
			$data = [];
			$rows	= $this->store->query($q);
			foreach($rows->result() as $row){
				$dateISO = new DateTime($row->date);
				
				$r ['contact_id'] = $row->contact_id;
				$r ['title']	= $row->title;
				$r ['purpose']	= $row->purpose;
				$r ['date']		= $dateISO->format(DateTime::ATOM);
				$r ['image']	= $this->_getReportImage($row->contact_id);
				$r ['marketer']	= $row->marketer_name;
				$r ['marketer_country'] = "Indonesia";
				$r ['marketer_position'] = "Marketing Officer";
				$r ['avatar']	= 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
				$r ['tags']		= $this->_tagstoArray($row->tags);
				
				array_push($data, $r);
			}
			
			$response = [
				'status'	=> true,
				'message'	=> "Contact report loaded ".$by,
				'data'		=> $data
			];
			
			return $this->response($response, 200);
			
		}
		
		public function by_get($by){

		    $ip = $this->input->ip_address();
		    $last_search = $this->store->query("SELECT * FROM tb_search WHERE ip='$ip' ORDER BY date DESC LIMIT 1");
		    if($last_search->num_rows() > 0){
                $awal  = strtotime($last_search->row()->date);
                $akhir = time();
                $total = ($akhir-$awal)/60;

                if($total > 5){
                    $this->store->insert('tb_search',['url' => current_url(), 'ip' => $ip, 'date' => date('Y-m-d H:i:s')]);
                }
            }else{
                $this->store->insert('tb_search',['url' => current_url(), 'ip' => $ip, 'date' => date('Y-m-d H:i:s')]);
            }

		
			switch($by){
				case 'company' :
					$q = "SELECT c.*
						FROM tb_contact_reports c
						GROUP BY c.title ORDER BY title DESC
						LIMIT 0, 10";
					
				break;
				case 'marketer':
					$q = "SELECT c.*
						FROM tb_contact_reports c
						GROUP BY c.marketer_name ORDER BY date DESC
						LIMIT 0, 20";
				break;
				case 'tags' : 
					$q = "SELECT *, (SELECT count(*) from tb_contact_reports c WHERE find_in_set(t.tags,c.tags)) as tot FROM tb_tags t ORDER BY tot DESC LIMIT 0,10";
				break;
				default:
					$q = null;
				break;
			
			}
			
			$data = [];
			
			$rows	= $this->store->query($q);
			switch($by){
				case 'company':
				
					foreach($rows->result() as $row){
						
						$r ['company']	= $row->title;
						$r ['contact_reports'] = $this->get_cr_by_comp($row->title);
						array_push($data, $r);
					}
					break;
					
				case 'marketer':
				
					foreach($rows->result() as $row){
						
						$r ['marketer_id']	= 0;
						$r ['marketer']		= $row->marketer_name;
						$r ['picture']		='https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
						$r ['contact_reports'] = $this->get_cr_by_marketer($row->marketer_name,200);
                        $r ['marketer_country'] = "Indonesia";
                        $r ['marketer_position'] = "Marketing Officer";
						array_push($data, $r);
					}
					break;
				case 'tags':
					foreach($rows->result() as $row){
						$r ['tags']	= $row->tags;
						$r ['contact_reports'] = $this->get_cr_by_tags($row->tags);
						array_push($data, $r);
					}
				break;
					
			}
			
			$response = [
				'status'	=> true,
				'message'	=> "Contact report loaded ".$by,
				'data'		=> $data
			];
			
			return $this->response($response, 200);
			
			
		}
		
		private function get_cr_by_tags($tags){
			$q = "SELECT c.*
						FROM tb_contact_reports c
						WHERE
						 find_in_set('$tags',tags)";
			$data = [];
			
			$rows	= $this->store->query($q);
			foreach($rows->result() as $row){
				$dateISO = new DateTime($row->date);
				
				$r ['contact_id'] = $row->contact_id;
				$r ['title']	= $row->title;
				$r ['purpose']	= $row->purpose;
				$r ['date']		= $dateISO->format(DateTime::ATOM);
				$r ['image']	= $this->_getReportImage($row->contact_id);
				$r ['marketer']	= $row->marketer_name;
				$r ['marketer_country'] = "Indonesia";
				$r ['marketer_position'] = "Marketing Officer";
				$r ['avatar']	= 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
				$r ['tags']		= $this->_tagstoArray($row->tags);
				
				array_push($data, $r);
			}
			
			return  $data;
		}
		
		private function get_cr_by_marketer($id, $limit = 200){
			$q = "SELECT c.*
						FROM tb_contact_reports c
						WHERE
						    c.marketer_name = '$id'
						LIMIT 0, $limit";
			$data = [];
			
			$rows	= $this->store->query($q);
			foreach($rows->result() as $row){
				$dateISO = new DateTime($row->date);

				$r ['contact_id'] = $row->contact_id;
				$r ['title']	= $row->title;
				$r ['purpose']	= $row->purpose;
				$r ['date']		= $dateISO->format(DateTime::ATOM);
				$r ['image']	= $this->_getReportImage($row->contact_id);
				$r ['marketer']	= $row->marketer_name;
				$r ['marketer_country'] = "Indonesia";
				$r ['marketer_position'] = "Marketing Officer";
				$r ['avatar']	= 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
				$r ['tags']		= $this->_tagstoArray($row->tags);
				
				array_push($data, $r);
			}
			
			return  $data;
		}
		
		private function get_cr_by_comp($title){
			
			$q = "SELECT c.*
						FROM tb_contact_reports c
						WHERE
						    c.title = '$title'
						LIMIT 0, 10";
			$data = [];
			
			$rows	= $this->store->query($q);
			foreach($rows->result() as $row){
				$dateISO = new DateTime($row->date);
				
				$r ['contact_id'] = $row->contact_id;
				$r ['title']	= $row->title;
				$r ['purpose']	= $row->purpose;
				$r ['date']		= $dateISO->format(DateTime::ATOM);
				$r ['image']	= $this->_getReportImage($row->contact_id);
				$r ['marketer']	= $row->marketer_name;
				$r ['marketer_country'] = "Indonesia";
				$r ['marketer_position'] = "Marketing Officer";
				$r ['avatar']	= 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
				$r ['tags']		= $this->_tagstoArray($row->tags);
				
				array_push($data, $r);
			}
			
			return  $data;
			
		}
		
		private function _getReportImage($id){
			$db = $this->store->select('tb_contact_image',['where'=> ['contact_id'=> $id]]);
			$images = [];
			foreach($db->result() as $row){
               // $f = $this->encrypt->encode($row->image_file);
				//array_push($images, base_url('upload/images/'.$row->image_file));
                array_push($images, site_url('index.php/files/images/'.$row->image_file));
			}
			
			return $images;
		}
		
		private function _tagstoArray($tags){

            $tagsArray = [];
		    if(strlen($tags) > 0){
                $tagsArray = explode(',', $tags);
            }

			return $tagsArray;
		}


		public function insert_post(){
		    $data_post = array(
                   'title'    		=> $this->post('title'),
               	   'purpose'		=> $this->post('purpose'),
               	   'tags'			=> $this->post('tags'),
               	   'marketer_id'	=> $this->post('marketer_id'),
               	   'date'			=> $this->post('date'),
               	   'date_created'	=> date('Y-m-d H:i:s'),
               	   'file_origin'	=> $this->post('file_origin')
            );

			$insert = $this->store->insert('tb_contact_reports', $data_post);
			
			$res = [
				'status'	=> true,
				'data'		=> $data_post,
				'message'	=> "Contact Insert"
			];

			if ($res) {
            	return $this->response($res, 200);
	        } else {
	            return $this->response($res, 502);
	        }
			
			
		}

		public function delete_delete($id){
			
			$row = $this->store->select('tb_contact_reports',['where' => ['contact_id' => $id]]);
			
			if($row->num_rows() <= 0){
				return $this->response(['status' => false, "message"=> "Unsuccess"], 502);
			}
			
			$db = $this->store->delete('tb_contact_reports', array('contact_id' => $id));

			if($db){
				$data 	= array('contact_id' => $id);
				
				$row = $row->row();
				$arr = explode(',', $row->file_origin);
				if(count($arr) > 0){
					foreach($arr as $file){
						if(file_exists($file))
							unlink($file);
					}
				}else{
					if(file_exists($row->file_origin))
							unlink($row->file_origin);
				}
				
				$image 	= $this->store->select('tb_contact_image',['where' => array('contact_id' => $id)]);
				foreach($image->result() as $r){
					if(file_exists("./upload/images/".$r->image_file))
						unlink("./upload/images/".$r->image_file);
                        unlink("./upload/images/tmp/".$r->image_file);
				}
				
				$this->store->delete('tb_contact_image',['contact_id' => $id]);
	    		
	    		$res = [
					'status'	=> true,
					'data'		=> $data,
					'message'	=> "Contact Report is Deleted"
				];
				return $this->response($res, 200);
			}else{
				return $this->response(['status' => false, "message"=> "Unsuccess"], 502);
			}
		}
		
		public function update_put(){
			   $id = $this->put('id');
			   $data = array(
			   		'title'		=>$this->put('title'),
			   		'purpose'	=>$this->put('purpose'),
			   		'tags'		=>$this->put('tags'),
			   		'marketer_name'=>$this->put('marketer'),
			   		'date'		=>$this->put('date')
				);
                
		        $update = $this->store->update('tb_contact_reports', $data, array('contact_id' => $id));
		        if ($update) {
		            $this->response(['status' => true, "message"=> "Contact Report is Updated"],200);
		        } else {
		            return $this->response(['status' => false,'message'=> "Unsuccess"], 502);
		        }
			
		}

		private function set_base64($path){

            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        }
		
		 
	}