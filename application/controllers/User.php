<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . 'vendor/autoload.php';
    require APPPATH . 'libraries/REST_Controller.php';
    require APPPATH . 'libraries/Format.php';
    use Restserver\Libraries\REST_Controller;
    use \Firebase\JWT\JWT;
   
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    class User extends REST_Controller {
        private $secretkey = '38c7ebf2ae708b746d82b84060b09922';
        function __construct($config = 'rest') {
            parent::__construct($config);
            
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
            $this->load->database();
            $this->load->model('Login_Model');
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
        }

        public function viewtokenfail($username){
            return $this->response(array('Status'=>False, 'message'=>'Fail Generated'), 502);
        }

        public function cektoken(){

            $this->load->model('Login_Model');

            $jwt = $this->input->get_request_header('Authorization');

            try {
                $decode = JWT::decode($jwt,$this->secretkey,array('HS256'));

                if ($this->Login_Model->is_valid_num($decode->username)>0) {
                    return true;
                }

            } catch (Exception $e) {
                return $this->response(array('status' => false ,'message' => 'Invalid Token',),502);
            }
        }

        function user_get($id = '') {
            // $this->cektoken();
                $table = 'tb_user';
                $get_id = 'id';
                if ($id == '') {
                    $data_load = $this->db->get($table)->result();
                } else {
                
                    $this->db->where($get_id, $id);
                    $data_load = $this->db->get($table)->result();
                    
                }

                $data = array('status'=>true,
                                'message'=>'success loaded',
                                'data' =>$data_load);
                $this->response($data, 200);
            
        }

        function user_post() { 
            
            $table = 'tb_user';
            $insert = $this->db->insert($table, $this->post());
            $id = $this->db->insert_id();
            if ($insert) {
                    $data_insert = $this->post();
  
            $this->response(array('status'=>true,'message'=>'success insert','data' => [$data_insert],), 200);
            } 
                else {
                    $this->response(array('status' => false, 'message'=>'insert failed', 'data' => [$data_insert],),520);
                }
        }

        function user_put($id = '') {
            //$this->cektoken();
            $table = 'tb_user';
            $get_id = 'id';
            $this->db->where($get_id, $id);
            $update = $this->db->update($table, $this->put());
            if ($update) {
                $data_update = $this->put();
                $this->response(array('status' => true, 'message' => 'success update','data' => [$data_update],), 200);
            } 
                else {
                    $this->response(array('status' => false,'message' => 'update failed','data' => [$data_update],), 502);
                }
        }

        function user_delete($id) {
          //  $this->cektoken();
            $get_id = 'id';
            $table  = 'tb_user';
            $this->db->where($get_id, $id);
            $delete = $this->db->delete($table);
            if ($delete) {
                $data_delete['id'] = $id;
                $this->response(array('status' => true, 'message' => 'success delete','data' => [$data_delete],), 200);
            } 
                else {
                    $this->response(array('status' => false,'message' => 'delete failed','data' => [$data_update],), 502);
                }
        }

    }
    ?>