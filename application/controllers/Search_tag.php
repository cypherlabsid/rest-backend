<?php defined('BASEPATH') OR exit('No direct script access allowed');
	header('Access-Control-Allow-Origin: *'); 
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	header('Access-Control-Allow-Headers: *');
	
    class Search_tag extends MY_Controller{
		
		public function __construct(){
			parent::__construct();
		}
		
		public function all_get(){
			$tags = $this->store->select('tb_tags', ['order_by' => "tags ASC"]);
			
			$data = [];
			foreach($tags->result() as $key=>$row){
				$r['id']  = $row->tags;
				$r['text'] = $row->tags;
				array_push($data, $r);
			}
			
			$res = [
				'status'	=> true,
				'data'		=> $data,
				'message'	=> "Tags loaded"
			];
			
			return $this->response($data, 200);
			
		}

		public function del_delete($id){
			$db = $this->store->delete('tb_tags', array('id' => $id));

			if($db){
				$data = array('id' => $id);
	    		
	    		$res = [
					'status'	=> true,
					'data'		=> $data,
					'message'	=> "Tags Deleted"
				];
				return $this->response($res, 200);
			}else{
				return $this->response(['status' => false, "message"=> "Unsuccess"], 502);
			}
		}
		
		public function get_get(){
			
			$tags = $this->store->select('tb_tags', ['order_by' => "tags ASC"]);
			
			$data = [];
			foreach($tags->result() as $key=>$row){
				$r ['no']	= $key+1;
				$r ['id']	= $row->id;
				$r ['name'] = $row->tags;
				array_push($data, $r);
			}
			
			$res = [
				'status'	=> true,
				'data'		=> $data,
				'message'	=> "Tags loaded"
			];
			
			return $this->response($res, 200);
			
		}
		
		public function add_post(){
			
			$this->form_validation->set_rules('tags','Tag','trim|required');
			if($this->form_validation->run() == TRUE){
				
				$comma = ',';
				$tag = $this->post('tags');

				 if( strpos($tag, $comma) !== false ) {
				     
				     $arr = explode(',', $tag);
				     foreach($arr as $r){
					     if($this->_check_avail($r)){
						     $insert =  $this->store->insert('tb_tags',['tags'=> $r]);
					     }
				     }
				     
				     $this->response(['status' => true, "message"=> "Success"],200);
				     
				 }else{
					 
					 if($this->_check_avail($tag)){
						$insert =  $this->store->insert('tb_tags',['tags'=> $tag]);
						
						$this->response(['status' => true, "message"=> "Success"],200);
					 }
				 }
				
			}else{
				return $this->response(['status' => false,'message'=> validation_errors()], 502);
			}
		}

		public function update_put($id){
			
			    $tags = $this->put('tags');
                

		        $query = "update tb_tags set tags='$tags' where id='$id' ";
		        $update = $this->store->query($query);
		        if ($update) {
		            $this->response(['status' => true, "message"=> "Success"],200);
		        } else {
		            return $this->response(['status' => false,'message'=> validation_errors()], 502);
		        }
				
		
				
			
		}

		
		private function _check_avail($tags){
			$data = $this->store->select('tb_tags',['where' => ['tags' => $tags]]);
			
			if($data->num_rows() > 0){
				return false;
			}else{
				return true;
			}
		}
		
	}
		
    ?>