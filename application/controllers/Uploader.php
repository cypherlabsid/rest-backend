<?php defined('BASEPATH') OR exit('No direct script access allowed');
	header('Access-Control-Allow-Origin: *'); 
	header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
	header('Access-Control-Allow-Headers: *');
	class Uploader extends MY_Controller{
		
		public function __construct(){
			parent::__construct();
			
			$method = $this->input->method(TRUE);
			
		    if($method == "OPTIONS") {
			   
		        die();
		        
		    }
		}
		
		public function submit_post(){


			$file_name = $_FILES['file']['name'];
			$original_name = $file_name;
    		$basename = substr($file_name,0,strpos($file_name,'.'));
    		
    		
		    if($_FILES['file']['type']=='application/pdf'){
			    $new_name = time().uniqid().$_FILES["file"]['name'];
			    
			    $config['upload_path']          = './upload/doc/';
			    $config['allowed_types']        = '*';
			    $config['file_name']			= $new_name;
			    

                $this->load->library('upload', $config);
                
                if(!$this->upload->do_upload('file')){
	                return $this->response($this->upload->display_errors(), 502);
                }
                
                $files = $this->upload->data();

		        $img = new imagick();

				$img->setResolution(300, 300);
				$img->readImage(FCPATH . 'upload/doc/'.$files['file_name']);
				$img->setImageBackgroundColor('white');
				
				$img->setImageCompressionQuality(80);
		    
		        $num_pages = $img->getNumberImages();
		        $images = NULL;
		        for($i = 0;$i < $num_pages; $i++) {         
		            $images[]=$files['file_name'].'-'.$i.'.png';
		            $img->setIteratorIndex($i);
		            $img->setImageFormat('png');
					$img->setImageAlphaChannel(11);  
		            $img->writeImage(FCPATH . 'upload/images/'.$files['file_name'].'-'.$i.'.png');
		        }
		        
		        $res = [
			        'images'	=> $images,
			        'parse'		=> $this->parse('pdf',FCPATH . 'upload/doc/'.$files['file_name'],$original_name),
			        'file_origin' => './upload/doc/'.$files['file_name']
		        ];
		        
		        $img->destroy();
		        
		        $this->response($res,200);
		        
		        
		    }else if($_FILES['file']['type']== "application/msword" || $_FILES['file']['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
			   
			    $config['upload_path']          = './upload/doc/';
			    $config['allowed_types']        = '*';

                $this->load->library('upload', $config);
                
	
                if(!$this->upload->do_upload('file')){
	                return $this->response($this->upload->display_errors(), 502);
                }
                
                $this->load->library('parsers');
               
                
                $files = $this->upload->data();
                
				$converted = '/upload/converted/'.$files['file_name'].'.pdf';
				$this->parsers->wordToPDF($files['file_name'],$config['upload_path'], $converted);
				
				
				$img = new imagick();

				$img->setResolution(300, 300);
				$img->readImage(FCPATH . 'upload/converted/'.$files['file_name'].'.pdf');
				$img->setImageBackgroundColor('white');
				
				$img->setImageCompressionQuality(80);
		    
		        $num_pages = $img->getNumberImages();
		        $images = NULL;
		        for($i = 0;$i < $num_pages; $i++) {         
		            $images[]=$files['file_name'].'-'.$i.'.png';
		            $img->setIteratorIndex($i);
		            $img->setImageFormat('png');
					$img->setImageAlphaChannel(11);  
		            $img->writeImage(FCPATH . 'upload/images/'.$files['file_name'].'-'.$i.'.png');
		        }

                $res = [
			        'images'	=> $images,
			        'parse'		=> $this->parse('pdf',FCPATH . 'upload/converted/'.$files['file_name'].'.pdf', $original_name),
			        'file_origin'=> $config['upload_path'].$files['file_name'].",".'./upload/converted/'.$files['file_name'].'.pdf'
		        ];
		        $img->destroy();
                $this->response($res,200);
		    }
		}


		public function multiple_post(){
            ini_set('max_execution_time', 300);

            if(!$this->_valid_uuid(json_encode($this->input->post()))){
                return $this->response(['status'=> true,'message'=> "Success, files saved"], 200);
            }

            foreach ($_FILES as $key => $image) {

                $_FILES['file[]']['name']       = $_FILES[$key]['name'];
                $_FILES['file[]']['type']       = $_FILES[$key]['type'];
                $_FILES['file[]']['tmp_name']   = $_FILES[$key]['tmp_name'];
                $_FILES['file[]']['error']      = $_FILES[$key]['error'];
                $_FILES['file[]']['size']       = $_FILES[$key]['size'];


                $file_name = $_FILES[$key]['name'];
                $original_name = $file_name;

                if($_FILES['file[]']['type']=='application/pdf'){
                    $new_name = time().uniqid().$_FILES["file[]"]['name'];

                    $config['upload_path']          = './upload/doc/';
                    $config['allowed_types']        = '*';
                    $config['file_name']			= $new_name;

                    $this->load->library('upload', $config);

                    if(!$this->upload->do_upload('file[]')){
                        return $this->response($this->upload->display_errors(), 502);
                    }

                    $filesh = $this->upload->data();

                    $images = $this->set_as_images(FCPATH . 'upload/doc/'.$filesh['file_name'],FCPATH . 'upload/images/'.$filesh['file_name'],$filesh['file_name'] );

                    $parse = $this->parse('pdf',FCPATH . 'upload/doc/'.$filesh['file_name'],$original_name);
					$file_origin = './upload/doc/'.$filesh['file_name'];
                    $this->save_multiple($images,$parse, $file_origin);

                }else if($_FILES['file[]']['type']=='application/msword' || $_FILES['file[]']['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){

                    $config['upload_path']          = './upload/doc/';
                    $config['allowed_types']        = '*';

                    $this->load->library('upload', $config);

                    if(!$this->upload->do_upload('file[]')){
                        return $this->response($this->upload->display_errors(), 502);
                    }

                    $this->load->library('parsers');


                    $filesh = $this->upload->data();

                    $converted = '/upload/converted/'.$filesh['file_name'].'.pdf';
                    $this->parsers->wordToPDF($filesh['file_name'],$config['upload_path'], $converted);

                    $images = $this->set_as_images(FCPATH . 'upload/converted/'.$filesh['file_name'].'.pdf',FCPATH . 'upload/images/'.$filesh['file_name'],$filesh['file_name'] );

                    $parse = $this->parse('pdf',FCPATH . 'upload/converted/'.$filesh['file_name'].'.pdf',$original_name);
                    $file_origin = $config['upload_path'] . $filesh['file_name'].',./upload/converted/'.$filesh['file_name'].'.pdf';
                    $this->save_multiple($images,$parse,$file_origin);
                }

            }

            return $this->response(['status' => true, 'message'=> 'Success, files saved', 'data'=> $parse],200);
        }

        private function save_multiple($images,$parse, $file_origin = ''){

		    $data = [
		        'title'             => $parse ['title'],
                'marketer_name'     => $parse ['marketer'],
                'purpose'			=> $parse ['purpose'],
                'full_text'         => $parse ['full_text'],
                'date'              => $parse ['date'],
                'date_created'		=> date('Y-m-d H:i:s'),
                'file_origin'		=> $file_origin
            ];

		    $id = $this->store->insert('tb_contact_reports',$data);

		    foreach($images as $r){
		        $files = [
		            'contact_id'    => $id,
                    'image_file'    => $r
                ];

		        $this->store->insert('tb_contact_image',$files);
            }

        }

        private function set_as_images($dir, $dest, $name){

            $img = new imagick();

            $img->setResolution(300, 300);
            $img->readImage($dir);
            $img->setImageBackgroundColor('white');

            $img->setImageCompressionQuality(80);

            $num_pages = $img->getNumberImages();
            $images = NULL;
            for($i = 0;$i < $num_pages; $i++) {
                $images[]= $name.'-'.$i.'.png';
                $img->setIteratorIndex($i);
                $img->setImageFormat('png');
                $img->setImageAlphaChannel(11);
                $img->writeImage($dest.'-'.$i.'.png');
            }
            $this->convertPNGto8bitPNG($images);
            $img->destroy();
            return $images;
        }

        function convertPNGto8bitPNG ($images) {

		    foreach($images as $r){
                $sourcePath = FCPATH . 'upload/images/'.$r;
                $destPath =  FCPATH . 'upload/images/tmp/'.$r;
                $srcimage = imagecreatefrompng($sourcePath);
                list($width, $height) = getimagesize($sourcePath);

                $img = imagecreatetruecolor($width, $height);
                $bga = imagecolorallocatealpha($img, 0, 0, 0, 127);
                imagecolortransparent($img, $bga);
                imagefill($img, 0, 0, $bga);
                imagecopy($img, $srcimage, 0, 0, 0, 0, $width, $height);
                imagetruecolortopalette($img, false, 255);
                imagesavealpha($img, true);

                imagepng($img, $destPath);
                imagedestroy($img);
            }


        }
		
		private function parse($type, $file, $original_name){
			
			$this->load->library('parsers');
			if($type == "pdf"){
				
				
				$pdf = $this->parsers->pdf->parseFile($file);
				$pages  = $pdf->getPages();
			
				$data = "";
				
				foreach ($pages as $page) {
				    $data .= $page->getText();
				}
				
//				$title = $this->get_string_between($data,'NAME OF COMPANY','CONTACT PERSON');
//                if(strlen($title) <= 3){
//                    $title = $this->get_string_between($data,'COMPANY NAME','CONTACT PERSON');
//                }
//
//				$titles = explode("\n", $title);
//				if(count($titles) > 1){
//					$title = $titles[0];
//				}

                $title = $this->get_titles($data);
				
				
				$parsing = [
					'title' 	=> $title,
					'marketer'	=> $this->get_marketers($original_name),
					'purpose'	=> $this->get_purpose($data),
					'full_text'	=> $data,
					'date'		=> $this->get_date_of_meeting($data)
					
				];
				
				return $parsing;
			}else if('docx'){
				
				$pdf = $this->parsers->pdf->parseFile($file);
				$pages  = $pdf->getPages();
			
				$data = "";
				
				foreach ($pages as $page) {
				    $data .= $page->getText();
				}

//				$title = $this->get_string_between($data,'NAME OF COMPANY','CONTACT PERSON');
//				if(strlen($title) <= 3){
//                    $title = $this->get_string_between($data,'COMPANY NAME','CONTACT PERSON');
//                }

                $title = $this->get_titles($data);

				$parsing = [
					'title' 	=> $title,
					'marketer'	=> $this->get_marketers($original_name),
					'purpose'	=> $this->get_purpose($data),
					'full_text'	=> $data,
					'date'		=> $this->get_date_of_meeting($data)
					
				];
				
				return $parsing;
				
			}
		}
		
		private function get_string_between($data, $start, $ends = NULL){
		   	$data = trim(preg_replace('/\t+/', '', $data));
		   	
		    $ini  = strpos($data, $start);
			$ini  += strlen($start);
			$text = substr($data,$ini);
			$text = str_replace(":", "", $text);
			
			if($ends == NULL){
				$last = trim($text);
				if(strlen($last) > 100){
					$last_pos = strpos($last, '(**)');
					if( $last_pos !== false){
						$last = trim(substr($last,0,$last_pos));
					}else{
						$last_pos = strpos($last, '(*)');
						$last = trim(substr($last,0,$last_pos));
					}
				}
				
				$lastArr = explode('*', $last);
				if(count($lastArr) > 1){
					$last = $lastArr[0];
				}
				
				return $last;
			}
			$end  = strpos($text, $ends);
			$text = trim(substr($text,0,$end));
			$textArr = explode('*', $text);
			if(count($textArr) > 1){
				$text = $textArr[0];
			}

			return $text;
		}

		private function get_titles($data){
            $data = trim(preg_replace('/\t+/', '', $data));
            $ini = preg_split("/name of company/i", $data);

            if(count($ini) < 2){
                $ini = preg_split("/company name/i", $data);
            }

            if(count($ini) >= 2){
                $title = $ini [1];
                $title = explode("\n", $title);
                if(count($title) > 0){
                    $title = trim(str_replace(":", "", $title[0]));

                }else{
                    $title = "";
                }

            }else{
                $title = "";
            }
            return $title;
        }
		
		private function get_date_of_meeting($data){
			$data = trim(preg_replace('/\t+/', '', $data));
			$ini = preg_split("/date of meeting/i", $data);
			if(count($ini) <= 1){
                $ini = preg_split("/meeting date/i", $data);
                if(count($ini) <= 1){
                    $ini = preg_split("/date of contact/i", $data);

                    if(count($ini) <= 1){
                        $ini = preg_split("/contact date/i",$data);
                    }
                }
            }

            if(count($ini) <= 1){
                $ini = preg_split("/visit date/i",$data);
            }

            if(count($ini) <= 1){
                $ini = preg_split("/visit of date/i",$data);
            }

            if(count($ini) <= 1){
                $ini = preg_split("/trip date/i",$data);
            }

            if(count($ini) <= 1){
                $ini = preg_split("/trip of date/i",$data);
            }

            if(count($ini) <= 1){
                $ini = preg_split("/date of call/i",$data);
            }

            if(count($ini) <= 1){
                $ini = preg_split("/call date/i",$data);
            }

			if(count($ini) >= 2){
				$date = $ini [1];

				$date = explode("\n", $date);

				if(count($date) > 0){
					$date = trim(str_replace(":", "", $date[0]));

					$date = $this->date_validation($date);
					
				}else{
					$date = date('Y-m-d');
				}
					
			}else{
				$date = date('Y-m-d');
			}
			return $date;
		}
		
		private function get_marketers($file){
		    $name = explode('-',$file);
		    if(count($name) >= 1){
		        $name = $name [0];
            }else{
		        $name = $file;
            }

			return $name;

//            $data = trim(preg_replace('/\t+/', '', $data));
//            $ini = preg_split("/reported by/i", $data);
//            if(count($ini) <= 0){
//                $ini = preg_split("/reported by/i", $data);
//            }
//
//
//            if(count($ini) >= 2){
//                $title = $ini [1];
//                $title = explode("\n", $title);
//                if(count($title) > 0){
//                    $title = trim(str_replace(":", "", $title[0]));
//
//                }else{
//                    $title = "";
//                }
//
//            }else{
//                $title = "";
//            }
//            return $title;
		}


		private function date_validation($date){

            $date = str_replace(",","","$date");
            $date = str_replace(".","","$date");

            $arr_and = explode("&", $date);
            if(count($arr_and) > 1){
                if(strlen($arr_and[0]) > 5){
                    $date = trim($arr_and[0]);
                }else{
                    $date = trim($arr_and[1]);
                }
            }
            $arr_date = explode(" ", $date);

            $tanggalnya = "";
            if(count($arr_date) >=3){
                for($i= 0; $i < 3; $i++){
                    if($i < 2){
                        $tanggalnya .= $arr_date[$i]."-";
                    }else{
                        $tanggalnya .= $arr_date[$i];
                    }

                }
            }


            $return_date  = "";

            try {
                $tanggalnya = date('Y-m-d', strtotime($tanggalnya));

                if(date('Y',strtotime($tanggalnya)) < 2000){
                    $return_date = date('Y-m-d');
                }else{
                    $return_date = $tanggalnya;
                }
            } catch(Exception $e) {
                $return_date = date('Y-m-d');
            }

            return $return_date;
        }
		
		private function get_purpose($data){
			$datacleartab = trim(preg_replace('/\t+/', ' ', $data));
			
			$datacleartab = str_replace("| PURPOSE OF MEETING | DISCUSSION OUTLINE", "", $datacleartab);
			$datacleartab = str_replace("| PURPOSE OF MEETING | DISCUSSION OUTLINE:", "", $datacleartab);
			$datalower = strtolower($datacleartab);
			
			$title	= "purpose of meeting | discussion outline";
			
			$title_position = strpos($datalower,$title );
			
			
			if($title_position !== false){
				$title_position  += strlen($title);
				$purposePoint = substr($datacleartab,$title_position);
				$purposePoint = str_replace(":", "", $purposePoint);
				
				$purpose_preg = trim(preg_replace('/\t+/', ' ', $purposePoint));
				$purpose_lower = strtolower($purpose_preg);
				
				$last_words = strpos($purpose_lower,"marketer’s");
				
				return trim(substr($purposePoint,0, $last_words));
				
			}else{
				$title = "purpose of meeting";
				$checktot = explode($title, $datalower);
				if($checktot > 2){
					$datalower 		= $title." ".$checktot[count($checktot)-1];
					
					$totChar = 0;
					for($i = 0; $i < count($checktot)-1; $i++){
						$totChar += strlen($checktot[$i]."purpose of meeting");
					}
					$datacleartab = substr($datacleartab,$totChar);
					$purposePoint = $datacleartab;
				}else{
					$title_position = strpos($datalower,$title );
				
					$title_position  += strlen($title);
				
					$purposePoint = substr($datacleartab,$title_position);
				}
				$purposePoint = str_replace(":", "", $purposePoint);
				
				$purpose_preg = trim(preg_replace('/\t+/', ' ', $purposePoint));
				$purpose_lower = strtolower($purpose_preg);
				
				$last_words = strpos($purpose_lower,"discussion outline");
				
				return trim(substr($purposePoint,0,$last_words));
			}
			
			
		}
		
		

	}