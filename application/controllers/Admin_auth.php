<?php
	
	class Admin_auth extends MY_Controller{
		
		public function __construct(){
			parent::__construct();
		}
		
		public function login_post(){
			$username = $this->post('username',TRUE); 
            $password = $this->post('password',TRUE);
            
            $login = $this->users->login($username, $password,false);
            if($login){
	            $user_info = $this->users->get_user();
	           
                $time = time();
                
                $payload = [
			        'iat'  => $time,
			        'jti'  => $user_info->id,
			        'nbf'  => $time, 
			        'exp'  => strtotime('+30 days'), 
			        'data' => [ 
			            'id'   => $user_info->id, 
			            'username' => $user_info->username,
			        ]
			    ];
                
                unset($user_info->pass);
                
                $output 				= (Array) $user_info;
                $output ['picture']		= strlen($user_info->picture) > 0 ? $user_info->picture : 'https://www.congresobioetica.com/wp-content/uploads/2017/03/avatar.png';
                $output ['exp']			= date('Y-m-d H:i:s',$payload['exp']);
                $output	['token'] 		= MY_Controller::$JWT->encode($payload,$this->secretkey);
                $data 					= ['status'=>true, 'message'=>"Login success", 'data'=>[$output]];
                
				$this->response($data, 200);
            }else{
	            $this->response(array('status'=>false, 'message'=>$this->users->errors[0]), 502);
            }
		}
		
		public function reset_password_post(){
			$rawData = file_get_contents("php://input");
			$arr = json_decode($rawData);
			
			$this->response(array('status'=>true, 'message'=> $arr), 200);
		}
		
	}