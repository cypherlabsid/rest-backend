<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'api';

$route['404_override'] = '';

$route['translate_uri_dashes'] = TRUE;

$route['api/example/users/(:num)'] = 'api/example/users/id/$1';

$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; 
