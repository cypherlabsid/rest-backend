<?php
	 
	require APPPATH . 'vendor/autoload.php';
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . 'libraries/Format.php';
    
    use Restserver\Libraries\REST_Controller;
    use \Firebase\JWT\JWT;
   
    header('Access-Control-Allow-Origin: *');
    //header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
    class MY_Controller extends REST_Controller{
	    
	    public $secretkey = '38c7ebf2ae708b746d82b84060b09922';
	    public static $JWT;
	    public $user_info;
	    public $message = "";
	    
	    public function __construct($authorize = false) {
            parent::__construct('rest');
          
            self::$JWT = new JWT();
	        $method = $_SERVER['REQUEST_METHOD'];
	        if ($method == "OPTIONS") {
	            die();
	        }
            $this->load->model('Login_Model');
			
			
           
            if($authorize){
	            if(!$this->authorize()){
		            $this->response(array('status' => false, 'message'=>$this->message),502);
	            }
            }
			
        }
        
        private function authorize(){
	        
	        $jwt = $this->input->get_request_header('Authorization');

            try {
                $decode 	= JWT::decode($jwt,$this->secretkey,array('HS256'));
				$login		= $this->users->login_fast($decode->data->id);
                if($login){
	                $this->user_info = $this->users->get_user();
	                return true;
                }else{
	                $this->message = $this->users->errors[0];
	                return false;
                }

            } catch (Exception $e) {
	            $this->message =  $e->getMessage();
	            return false;
            }
        }
        
        
        public function _valid_uuid($params){
	        $header = getallheaders();
	        $log = $this->store->select('tb_logger',['where'=> ['uuid'=> $header['UUID']]]);
	        if($log->num_rows() > 0){
		        return false;
	        }else{
		        
		        $header = getallheaders();
				$data = [
					'uuid'		=> $header['UUID'],
					'log_data'	=> $params,
					'date'		=> date('Y-m-d H:i:s')
				];
				$this->store->insert('tb_logger',$data);
		        
		        return true;
	        }
        }
        
        
        
       
    }