<?php 
	
	require APPPATH.'libraries/docx/vendor/autoload.php';
	require APPPATH.'libraries/pdf/vendor/autoload.php';
	require APPPATH.'libraries/unoconv/Unoconv.php';
	use Unoconv\Unoconv;
	
	
	class Parsers{
		
		public $docx;
		public $pdf;
		
		public function __construct(){
			$this->pdf = new \Smalot\PdfParser\Parser();
			$this->docx = new \LukeMadhanga\DocumentParser();
		}
		
		public function wordToPDF($file_name,$loc, $saveTo){
			
			$location = pathROOT().str_replace("./","/", $loc.$file_name);
			
			$parse = Unoconv::convertToPdf($location, pathROOT().$saveTo);
			print_r($parse);
		}
		
	}