<?php
	
	require APPPATH.'libraries/php-mailer/vendor/autoload.php';
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	
	class Emailer{
		
		public $email;
		
		public function __construct(){
			$this->email = new PHPMailer(true); 
		}
		
		public function mailSend($email, $subject, $body, $name="Sembcorp user"){
			$mail = $this->email;
			try {
			    
			    $mail->isSMTP();                                      // Set mailer to use SMTP
			    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			    $mail->SMTPAuth = true;                               // Enable SMTP authentication
			    $mail->Username = 'gem.app.sg@gmail.com';                 // SMTP username
			    $mail->Password = 'cypherlabs2018';                           // SMTP password
			    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			    $mail->Port = 465;                                    // TCP port to connect to
			
			    //Recipients
			    $mail->setFrom('gem.app.sg@gmail.com', 'GEM Report App');
			    $mail->addAddress($email,$name); 
		
			
			    $mail->isHTML(true);
			    $mail->Subject = $subject;
			    $mail->Body    = $body;
			 
			
			    $mail->send();
			    
			} catch (Exception $e) {
			    
			}
			
		}
		
		public function register_body($data = []){
			
			$html = '<!DOCTYPE html>
			<html lang="en" >
			
			<head>
			  <meta charset="UTF-8">
			  <title>GEM Apps</title>
			  
			  
			  <style>
			  @media only screen and (max-width: 600px) {
				.main {
					width: 320px !important;
				}
		
				.top-image {
					width: 100% !important;
				}
				.inside-footer {
					width: 320px !important;
				}
				table[class="contenttable"] { 
		            width: 320px !important;
		            text-align: left !important;
		        }
		        td[class="force-col"] {
			        display: block !important;
			    }
			     td[class="rm-col"] {
			        display: none !important;
			    }
				.mt {
					margin-top: 15px !important;
				}
				*[class].width300 {width: 255px !important;}
				*[class].block {display:block !important;}
				*[class].blockcol {display:none !important;}
				.emailButton{
		            width: 100% !important;
		        }
		
		        .emailButton a {
		            display:block !important;
		            font-size:18px !important;
		        }
		
			}
			</style>
			
			  
			</head>
			
			<body>
			
			  
			  <body link="#00a5b5" vlink="#00a5b5" alink="#00a5b5">
			
			<table class=" main contenttable" align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
					<tr>
						<td class="border" style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">
							<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
								<tr>
									<td colspan="4" valign="top" class="image-section" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">
										<a href="#"><img class="top-image" src="https://s3-ap-southeast-1.amazonaws.com/web.reportgem.com/assets/img/logo_horizontal.png" style="line-height: 1;max-width:120px;margin:10px 5px" alt="GEM App"></a>
									</td>
								</tr>
								<tr>
									<td valign="top" class="side title" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
										<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
											<tr>
												<td class="head-title" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;">
													<div class="mktEditable" id="main_title">
														Welcome to GEM Report App 
													</div>
												</td>
											</tr>
											<tr>
												<td class="sub-title" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;padding-top:5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 18px;line-height: 29px;font-weight: bold;text-align: center;">
												<div class="mktEditable" id="intro_title">
													Contact report management
												</div></td>
											</tr>
											<tr>
												<td class="top-padding" style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;"></td>
											</tr>
											<tr>
												<td class="grey-block" style="border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:center;">
												<div class="mktEditable" style="margin-bottom: 30px" id="cta">
												
													<strong>Username :</strong> '.$data ['username'].'<br>
						                            <strong>Email </strong>: '.$data ['email'].'<br><br>
						                            
						                            <div style="display:block; text-align:left;margin-bottom:10px">
														Hello '.$data ['first_name'].',<br><br>
														Thank you for creating an GEM App account. Please login to your account via Android/iOS app.
													</div>
						                             
												</div>
												</td>
											</tr>
											
											
										</table>
									</td>
								</tr>
								
								<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">
									<td valign="top" class="footer" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
										<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
											<tr>
												<td class="inside-footer" align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
			<div id="address" class="mktEditable">
													<b>GEM </b>App 2018<br>
			                          
			                            
			</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			  </body>
			  
			  
			
			</body>
			
			</html>';
			
			return $html;
		}
		
		public function reset_password($data){
			$html = '<!DOCTYPE html>
			<html lang="en" >
			
			<head>
			  <meta charset="UTF-8">
			  <title>GEM App</title>
			  
			  
			  <style>
			  @media only screen and (max-width: 600px) {
				.main {
					width: 320px !important;
				}
		
				.top-image {
					width: 100% !important;
				}
				.inside-footer {
					width: 320px !important;
				}
				table[class="contenttable"] { 
		            width: 320px !important;
		            text-align: left !important;
		        }
		        td[class="force-col"] {
			        display: block !important;
			    }
			     td[class="rm-col"] {
			        display: none !important;
			    }
				.mt {
					margin-top: 15px !important;
				}
				*[class].width300 {width: 255px !important;}
				*[class].block {display:block !important;}
				*[class].blockcol {display:none !important;}
				.emailButton{
		            width: 100% !important;
		        }
		
		        .emailButton a {
		            display:block !important;
		            font-size:18px !important;
		        }
		
			}
			</style>
			
			  
			</head>
			
			<body>
			
			  
			  <body link="#00a5b5" vlink="#00a5b5" alink="#00a5b5">
			
			<table class=" main contenttable" align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
					<tr>
						<td class="border" style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">
							<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
								<tr>
									<td colspan="4" valign="top" class="image-section" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">
										<a href="#"><img class="top-image" src="https://s3-ap-southeast-1.amazonaws.com/web.reportgem.com/assets/img/logo_horizontal.png" style="line-height: 1;max-width:120px;margin:10px 5px" alt="GEM App"></a>
									</td>
								</tr>
								<tr>
									<td valign="top" class="side title" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
										<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
											<tr>
												<td class="head-title" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;">
													<div class="mktEditable" id="main_title">
														Reset Password
													</div>
												</td>
											</tr>
											<tr>
												<td class="sub-title" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;padding-top:5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 18px;line-height: 29px;font-weight: bold;text-align: center;">
												<div class="mktEditable" id="intro_title">
													<br>
												</div></td>
											</tr>
											<tr>
												<td class="top-padding" style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;"></td>
											</tr>
											<tr>
												<td class="grey-block" style="border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:center;">
												<div class="mktEditable" id="cta" style="text-align:left">
												
													<strong>Username :</strong> '.$data ['username'].'<br>
						                            <strong>Email </strong>: '.$data ['email'].'<br>
						                            <strong>Time </strong>: '.$data ['time_req'].'<br>
						                            <strong>Request from </strong>: '.$data ['agent'].'<br><br>
						                            
						                            <div style="display:block; text-align:left;margin-bottom:10px">
														Hello '.$data ['first_name'].',<br><br>
														We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below.
														Open the GEM Report App then put verification Code :<br>
															<strong style="text-align:center"> '.$data ['ver_code'].' </strong><br><br>
														<small>
														GEM will never e-mail you and ask you to disclose or verify your GEM password. If you receive a suspicious e-mail with this activity, please update your account information, do not put verification code. Thanks for using GEM App!
														<small>
														<br><br>
														<small style="font-style:italic"> *) verification code is only valid for 15 minutes from the first time you request </small>
													</div>
						                             
												</div>
												</td>
											</tr>
											
											
										</table>
									</td>
								</tr>
								
								<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">
									<td valign="top" class="footer" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
										<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
											<tr>
												<td class="inside-footer" align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
			<div id="address" class="mktEditable">
													<b>GEM </b>App 2018<br>
			                          
			                            
			</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			  </body>
			  
			  
			
			</body>
			
			</html>';
			
			return $html;
		}


        public function reset_password_adm($data){
            $html = '<!DOCTYPE html>
			<html lang="en" >
			
			<head>
			  <meta charset="UTF-8">
			  <title>GEM App</title>
			  
			  
			  <style>
			  @media only screen and (max-width: 600px) {
				.main {
					width: 320px !important;
				}
		
				.top-image {
					width: 100% !important;
				}
				.inside-footer {
					width: 320px !important;
				}
				table[class="contenttable"] { 
		            width: 320px !important;
		            text-align: left !important;
		        }
		        td[class="force-col"] {
			        display: block !important;
			    }
			     td[class="rm-col"] {
			        display: none !important;
			    }
				.mt {
					margin-top: 15px !important;
				}
				*[class].width300 {width: 255px !important;}
				*[class].block {display:block !important;}
				*[class].blockcol {display:none !important;}
				.emailButton{
		            width: 100% !important;
		        }
		
		        .emailButton a {
		            display:block !important;
		            font-size:18px !important;
		        }
		
			}
			</style>
			
			  
			</head>
			
			<body>
			
			  
			  <body link="#00a5b5" vlink="#00a5b5" alink="#00a5b5">
			
			<table class=" main contenttable" align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;font-family: Arial, sans-serif;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
					<tr>
						<td class="border" style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;">
							<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
								<tr>
									<td colspan="4" valign="top" class="image-section" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #00a5b5">
										<a href="#"><img class="top-image" src="https://s3-ap-southeast-1.amazonaws.com/web.reportgem.com/assets/img/logo_horizontal.png" style="line-height: 1;max-width:120px;margin:10px 5px" alt="GEM App"></a>
									</td>
								</tr>
								<tr>
									<td valign="top" class="side title" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
										<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
											<tr>
												<td class="head-title" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;">
													<div class="mktEditable" id="main_title">
														Reset Password
													</div>
												</td>
											</tr>
											<tr>
												<td class="sub-title" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;padding-top:5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 18px;line-height: 29px;font-weight: bold;text-align: center;">
												<div class="mktEditable" id="intro_title">
													<br>
												</div></td>
											</tr>
											<tr>
												<td class="top-padding" style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;"></td>
											</tr>
											<tr>
												<td class="grey-block" style="border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background-color: #fff; text-align:center;">
												<div class="mktEditable" id="cta" style="text-align:left">
												
													<strong>Username :</strong> '.$data ['username'].'<br>
						                            <strong>Email </strong>: '.$data ['email'].'<br>
						                            <strong>Time </strong>: '.$data ['time_req'].'<br>
						                            <strong>Request from </strong>: '.$data ['agent'].'<br><br>
						                            
						                            <div style="display:block; text-align:left;margin-bottom:10px">
														Hello '.$data ['first_name'].',<br><br>
														We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below.
														Please open link below to reset your password :<br>
															<a href="'.$data ['web_url'].'"><strong style="text-align:center"> '.$data ['web_url'].' </strong></a><br><br>
														<small>
														GEM will never e-mail you and ask you to disclose or verify your GEM password. If you receive a suspicious e-mail with this activity, please update your account information. Thanks for using GEM App!
														<small>
														<br><br>
														
													</div>
						                             
												</div>
												</td>
											</tr>
											
											
										</table>
									</td>
								</tr>
								
								<tr bgcolor="#fff" style="border-top: 4px solid #00a5b5;">
									<td valign="top" class="footer" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
										<table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;font-family: Arial, sans-serif;">
											<tr>
												<td class="inside-footer" align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-family: Arial, sans-serif;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
			<div id="address" class="mktEditable">
													<b>GEM </b>App 2018<br>
			                          
			                            
			</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			  </body>
			  
			  
			
			</body>
			
			</html>';

            return $html;
        }
	}
