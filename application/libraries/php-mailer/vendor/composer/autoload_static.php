<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc647afb9b19ab8c3a2a01d960f448cea
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc647afb9b19ab8c3a2a01d960f448cea::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc647afb9b19ab8c3a2a01d960f448cea::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
